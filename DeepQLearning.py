import numpy as np
from random import choice, random
from tensorflow import keras

room_width = 10 # 10 tiles
actions = 0, 1 # Forward and Backward
state = np.zeros((room_width,))
score_table = np.zeros((room_width,))
# Defining scores:
score_table[0] = 2 # First tile
score_table[-1:] = 10 # Last tile
# Initial state of player
cursor = 0 
state[cursor] = 1

# state in
# action out
model = keras.Sequential([
    keras.layers.Dense(5, activation="relu", input_shape=(room_width,)),
    keras.layers.Dense(len(actions), activation="sigmoid"),
])
model.compile(optimizer="sgd", loss="mean_squared_error", metrics=["accuracy"])

"""
    Rules:
        - Backward action redirects player to start point
        - Forward action is 1 tile going forward
"""

def get_score(state, action, lr, discount, reward):
    current_qvalues = model.predict(np.array([state]))[0].tolist()
    if state[room_width-1] == 1:
        next_qvalues = current_qvalues[:]
    else:
        next_state = state[:]
        next_state[np.argmax(state)] = 0
        next_state[np.argmax(state)+1] = 1
        next_qvalues = model.predict(np.array([next_state]))[0].tolist()
    
    action_qvalue = current_qvalues[action]
    next_state_action_qvalue = next_qvalues[action]
    new_action_qvalue = action_qvalue + lr * (reward + discount * \
        next_state_action_qvalue - action_qvalue)
    
    output = np.zeros((len(actions)))
    for i in range(0, len(output)):
        if i == action:
            output[i] = new_action_qvalue
        else:
            output[i] = 0
    
    return output

# Training part :
lr = 0.4
discount = 0.9
epochs = 100
for step in range(epochs):
    current_action = choice(actions)
    last_cursor = cursor

    if current_action == 0:
        if cursor<room_width-1:
            cursor += 1
        else:
            cursor = room_width-1
    elif current_action == 1:
        cursor = 0

    state[last_cursor] = 0
    state[cursor] = 1
    reward = score_table[cursor]

    scores = get_score(state, current_action, lr, discount, reward)
    
    model.fit(np.array([state]), np.array([scores]), epochs=10)
