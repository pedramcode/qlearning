import numpy as np
from random import choice, random

room_width = 10 # 10 tiles
actions = 0, 1 # Forward and Backward
np.set_printoptions(suppress=True) # Normal numbers

qtable = np.zeros((room_width, len(actions)), dtype='float32')
score_table = np.zeros((room_width,))

# Defining scores:
score_table[0] = 2 # First tile
score_table[-1:] = 10 # Last tile

cursor = 0 # Initial state of player

"""
    Rules:
        - Backward action redirects player to start point
        - Forward action is 1 tile going forward
"""

def get_score(state, action, lr, discount, reward):
    current_qrow = qtable[state][action]
    if state < room_width-1:
        next_qrow = qtable[state+1][action]
    else:
        next_qrow = qtable[room_width-1][action]

    return current_qrow + lr * (reward + discount * next_qrow - current_qrow)

# Training part :
lr = 0.4
discount = 0.9
epochs = 20000
for step in range(epochs):
    current_action = choice(actions)
    last_cursor = cursor

    if current_action == 0:
        if cursor<room_width-1:
            cursor += 1
        else:
            cursor = room_width-1
    elif current_action == 1:
        cursor = 0

    reward = score_table[cursor]
    qtable[last_cursor][current_action] = get_score(last_cursor, current_action, \
        lr, discount, reward)

    print(qtable)